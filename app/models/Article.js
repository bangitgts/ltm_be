const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var mongooseDelete = require("mongoose-delete");
const AutoIncrement = require("mongoose-sequence")(mongoose);
const ArticleSchema = new Schema(
  {
    _id: {
      type: Number,
    },
    title: {
      type: String,
    },
    localStamp: {
      type: String,
    },
    description: {
      type: String,
    },
    image: {
      type: String,
    },
    linkArticle: {
      type: Date,
    },
  },
  {
    _id: false,
    collection: "Article",
  }
);

// Add plugin
ArticleSchema.plugin(AutoIncrement, { id: "Article", inc_field: "_id" });
ArticleSchema.plugin(mongooseDelete, { deleteAt: true, overrideMethods: "all" });
module.exports = mongoose.model("Article", ArticleSchema);
