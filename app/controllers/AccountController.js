const AccountModel = require("../models/Account");

const md5 = require("md5");
const jwt = require("jsonwebtoken");
class AccountController {
  async loginAccount(req, res) {
    try {
      // req.headers['content-type'] = 'application/json';
      let username = req.body.username;
      let password = md5(req.body.password);
      const checkLogin = await AccountModel.findOne({
        username: username,
        password: password,
      }).select("-__v");

      if (checkLogin) {
        let token = jwt.sign(
          {
            exp: Math.floor(Date.now() / 1000) + 60 * 60,
            _id: checkLogin._id,
          },
          "password"
        );
        checkLogin.token = token;
        res.header("auth-token", token);

        return res.status(200).json({
          message: "Loggin successfully",
          data: Object.assign(checkLogin._doc, { token: token }),
          success: true,
          status: 200,
        });
      } else {
        return res.status(400).json({
          message: "Loggin failed. Account or password does not match",
          success: false,
          status: 400,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Server Error",
        success: false,
        status: 500,
      });
    }
  }

  async registerUser(req, res) {
    let fullName = req.body.fullName;
    let username = req.body.username;
    let password = md5(req.body.password);
    let role = req.body.role;
    let gender = parseInt(req.body.gender);
    try {
      console.log(fullName);
      if (gender === 0 || gender === 1) {
        const checkUsername = await AccountModel.findOne({
          username: username,
        });
        if (!checkUsername) {
          AccountModel.create({
            fullName: fullName,
            username: username,
            password: password,
            role: role,
            gender: gender,
          });
          return res.status(200).json({
            message: "Account created successfully",
            status: 200,
            success: true,
          });
        } else {
          res.status(402).json({
            status: 402,
            success: false,
            message: "This account has been created",
          });
        }
      } else {
        res.status(400).json({
          status: 400,
          success: false,
          message: "Input data gender is incorrect",
        });
      }
    } catch (error) {
      res.status(500).json({
        status: 500,
        success: false,
        err: error,
        message: "Register failed because of server error",
      });
    }
  }

  async changePassword(req, res) {
    let password = req.body.password;
    let newpassword = req.body.newpassword;
    try {
      const idUser = await AccountModel.findOne({ _id: req.user._id });
      if (md5(password) === idUser.password) {
        idUser.password = md5(newpassword);
        idUser.save();
        return res.status(200).json({
          message: "Password change successfully",
          success: true,
          status: 200,
        });
      } else {
        return res.status(402).json({
          message: "Old password is incorrect",
          success: false,
          status: 400,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Sever error",
        success: false,
        status: 500,
      });
    }
  }
}

module.exports = new AccountController();
