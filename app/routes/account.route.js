const express = require("express");
const router = express.Router();
const checkToken = require("../auth/CheckToken");
const accountController = require("../controllers/AccountController");

router.post("/Login", accountController.loginAccount);
router.post("/Register", accountController.registerUser);
router.put("/ChangePassword",checkToken,accountController.changePassword);

module.exports = router;
