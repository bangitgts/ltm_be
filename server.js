const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const db = require("./config/connectDb.js");
const route = require("./app/routes");
const bodyParser = require("body-parser");
const cheerio = require("cheerio");
const request = require("request-promise");
const app = express();
const http = require("http");
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);
// Parse application/x-www-form-urlencoded
// for parsing application/json
app.use(bodyParser.json());
// for parsing application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

// for parsing multipart/form-data
route(app);

db.connect();
app.use(morgan("combined"));

app.get("/", (req, res) => {});

io.on("connection", () => {
  const getDataOnTheThao = (req, res) =>
    request("https://vnexpress.net/the-thao", (error, response, html) => {
      var jsonForWeb = [];
      if (!error && response.statusCode == 200) {
        $ = cheerio.load(html);
        let data = $(html).find(".wrapper-topstory-folder article.item-news.full-thumb.article-topstory");
        let dataSub = $(html).find("ul.list-sub-feature li");
        data.each((index, ele) => {
          const title = $(ele).find("h3.title-news a").text();
          const localStamp = $(ele).find("p.description a span").text();
          const description = $(ele).find("p.description a").text();
          const image = $(ele).find("div.thumb-art picture img").attr("src");
          const linkArticle = $(ele).find("div.thumb-art a").attr("href");
          const item = {
            title: title,
            localStamp: localStamp,
            description: description,
            image: image,
            linkArticle: linkArticle,
          };
          jsonForWeb.push(item);
        });
        dataSub.each((index, ele) => {
          const title = $(ele).find("h3.title_news a").attr("title");
          const localStamp = $(ele).find("p.description a span").text();
          const description = $(ele).find("p.description a").text();
          const image = $(ele).find("div.thumb-art picture img").attr("src");
          const linkArticle = $(ele).find("p.description a").attr("href");
          const item = {
            title: title,
            localStamp: localStamp,
            description: description,
            image: image,
            linkArticle: linkArticle,
          };
          jsonForWeb.push(item);
        });
      } else {
        console.log(error);
      }
      io.sockets.emit("getData", jsonForWeb);
    });
  getDataOnTheThao();
  setInterval(() => {
    getDataOnTheThao();
  }, 60000);
});

app.get("/test", (req, res) => {
  request("http://worldclockapi.com/api/json/est/now", (error, response, html) => {
    res.json(response.body);
  });
});

server.listen(3005, function () {
  var host = server.address().address;
  var port = server.address().port;
  console.log("Example app listening at http://%s:%s", host, port);
});
